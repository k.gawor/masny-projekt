import turtle
import math
szerokosc = 600
wysokosc = 200
drzwi_szer = 50
drzwi_wys = 90
ramie_trojkata = szerokosc/math.sqrt(2)
turtle.setworldcoordinates(-400, 0, 400, 800)
turtle.forward(drzwi_szer)
turtle.left(90)
turtle.forward(drzwi_wys)
turtle.left(90)
turtle.forward(drzwi_szer)
turtle.left(90)
turtle.forward(drzwi_wys)
turtle.left(90)
turtle.forward(szerokosc/2)
turtle.left(90)
turtle.forward(wysokosc)
turtle.left(45)
turtle.forward(ramie_trojkata)
turtle.left(90)
turtle.forward(ramie_trojkata)
turtle.left(45)
turtle.forward(wysokosc)
turtle.left(90)
turtle.forward(szerokosc)
turtle.left(90)
turtle.forward(wysokosc)
turtle.left(90)
turtle.forward(szerokosc)
turtle.exitonclick()