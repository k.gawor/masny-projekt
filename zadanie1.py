#zadanie 1
student1 = int(input("Wpisz swoj wiek"))
student2 = int(input("Wpisz swoj wiek"))

zdanie1 = "Pierwszy student jest starszy i ma "+ str(student1) +"  lat(a)"
zdanie2 = "Drugi student jest starszy i ma "+ str(student2) +" lat(a)"

if(student1>student2):
    print(zdanie1)
    with open("wiek1.txt", "a") as plik1:
        plik1.write(zdanie1)
else:
        print(zdanie2)
        with open("wiek1.txt", "a") as plik2:
            plik2.write(zdanie2)

#zadanie 2
student3 = int(input("Podaj wiek: "))
with open("wiek_drugiego_studenta.txt") as wiek:
    student4 = wiek.read()


if(student3<int(student4)):
    roznica = int(student4) - student3
    zdanie = "Pierwszy student jest mlodszy od studenta drugiego o " + str(roznica) + " lat"
    print(zdanie)
else:
    roznica2 = student3 - int(student4)
    zdanie = "Drugi student jest mlodszy od studenta pierwszego o " + str(roznica2) + " lat"
    print(zdanie)

if(student3>int(student4)):
    with open("wiek2.txt","a") as plik1:
        plik1.write(zdanie) 
else:
    with open("wiek2.txt","a") as plik2:
        plik2.write(zdanie)
