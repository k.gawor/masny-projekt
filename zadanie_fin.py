import csv
import requests
import datetime
import time
import numpy as np

#zaczytujemy stacje
stacje = requests.get("https://danepubliczne.imgw.pl/api/data/synop/format/csv")
data = stacje.content.decode()

lista_stacji = csv.reader(data.splitlines(), delimiter=',')

# nagłówki
headers = next(lista_stacji)
print(f"{headers[0]}:{headers[1]}")

#lista ze stacjami synoptycznymi i kodami
stations = {}

# wczytujemy reszte danych
for row in lista_stacji:
    code = row[0]
    station = row[1]
    stations[code] = station

for code, station in stations.items():
    print(f"{code}: {station}")

#wybór stacji
selected_code = input("Wybierz stację synoptyczną (wpisz kod): ")

# sprawdzenie czy wpisany kod istnieje
if selected_code not in stations:
    print(f"Nie ma stacji o podanym kodzie: {selected_code}")
    exit()
#link do pobierania danych
url_stacji=("https://danepubliczne.imgw.pl/api/data/synop/format/csv/id/" + selected_code)


# data i godzina, do której program bedzie gromadzić dane
try:
    end_time_str = input("Wpisz datę i godzinę, do której będzie działać program (format: RRRR-MM-DD HH:MM:SS): ")
    end_time = datetime.datetime.strptime(end_time_str, "%Y-%m-%d %H:%M:%S")
except ValueError:
    print("Błędny format daty lub godziny.")
    exit()
# Tworzenie tablicy do gromadzenia pomiarów
lista_srednia= []
# zaczytujemy aktualne dane
dane_stacja = requests.get(url_stacji)
lista_stacja = dane_stacja.text.splitlines()
reader_stacja = csv.reader(lista_stacja, delimiter=",")
headers2 = next(reader_stacja)
for row in reader_stacja:
    temperatura = row[4]
    lista_srednia.append(temperatura)
#petla zaczytujaca dane w przyszłosci
while datetime.datetime.now() < end_time:
    dane_w_petli = requests.get(url_stacji)
    lista_w_petl = dane_w_petli.text.splitlines()
    reader_w_petli = csv.reader(lista_w_petl, delimiter=",")
    headers_w_petli = next(reader_w_petli)
    for row in reader_w_petli:
        temperatura_np = row[4]
        if temperatura_np != lista_srednia[-1]:
            lista_srednia.append(temperatura_np)
            print(f"Dodano nowe dane: {temperatura_np}")
            time.sleep(3600)
        else:
            time.sleep(300)
            print("Brak nowych danych")

print("Odczytywanie danych zostało zakończone.")
print(f"Odnotowane temperatury: ", lista_srednia)


print(input("Wciśnij Enter aby kontynuować"))

# oblicznie sredniej z zaczytanych informacji
tablica_temperatury = np.array(lista_srednia)
tablica_liczbowa = tablica_temperatury.astype(float)
srednia_temperatura = tablica_liczbowa.mean()
print(f"Średnia temperatura na danej stacji w określonym czasie wynosi:", srednia_temperatura)


#zaczytywanie do pliku
with open("Wyniki.txt","a") as plik:
   plik.write(f'\n{srednia_temperatura}')
